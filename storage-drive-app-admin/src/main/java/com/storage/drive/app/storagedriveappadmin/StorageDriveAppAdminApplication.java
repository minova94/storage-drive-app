package com.storage.drive.app.storagedriveappadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StorageDriveAppAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(StorageDriveAppAdminApplication.class, args);
	}

}
