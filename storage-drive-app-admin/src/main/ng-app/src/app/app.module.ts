import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from "@angular/common/http";

import {AuthServiceConfig, GoogleLoginProvider, SocialLoginModule} from 'angularx-social-login';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import {ConfigService} from './login/config/config.service';
import {LoginService} from "./login/login.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    SocialLoginModule,
    HttpClientModule,
    RouterModule.forRoot([])
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: initConfig
    },
    ConfigService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function initConfig() {
  let config = new AuthServiceConfig([
    {
      id: GoogleLoginProvider.PROVIDER_ID,
      provider: new GoogleLoginProvider('519216935031-jiqojdq66l7njoqd2rh078puthpu83ps.apps.googleusercontent.com')
    }]);

  return config;
}
