import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class LoginService {

  constructor(private httpClient: HttpClient) {
  }

  sendIdToken(idToken: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this.httpClient.post('/api/user/authenticate', idToken, { headers });
  }

  authorize(authToken: string) {
    return this.httpClient.get('/api/drive/allFolders', { responseType: 'text'}).pipe(map(data => JSON.stringify(data)));
  }
}
