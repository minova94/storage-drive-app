import {Component, OnInit} from '@angular/core';
import {AuthService, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {ConfigService} from './config/config.service';
import {LoginService} from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private socialUser: SocialUser;
  private loggedIn: boolean;
  private isEmailVerified: boolean;
  private isLoadingSignOut: boolean;
  private isLoadingSignIn: boolean;

  constructor(private authService: AuthService, private configService: ConfigService, private loginService: LoginService) {
    this.isLoadingSignOut = false;
    this.isLoadingSignIn = false;
  }

  ngOnInit() {
    this.checkUser();
  }

  checkUser() {
    setTimeout(() => {
      this.authService.authState.subscribe((user) => {
        this.socialUser = user;
        this.loggedIn = (user != null);
        if (this.loggedIn) {
          this.isVerified(this.socialUser.idToken);
        }
      });
    }, 2000);
  }

  signInWithGoogle() {
    this.isLoadingSignIn = true;
    setTimeout(() => {
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData => {
        console.log(userData);
        this.isVerified(userData.idToken);
        this.loginService.authorize(userData.authToken).subscribe(data => {
          console.log(data);
        });
      }));
      this.isLoadingSignIn = false;
    }, 2000);
  }

  signOut() {
    this.isLoadingSignOut = true;
    setTimeout(() => {
      this.authService.signOut().then(data => {
        this.isEmailVerified = false;
      });
      this.isLoadingSignOut = false;
    }, 2000);
  }

  isVerified(idToken: string) {
    if (idToken != null) {
      this.loginService.sendIdToken(idToken).subscribe((data: boolean) => {
        this.isEmailVerified = data;
      });
    }
  }

}
