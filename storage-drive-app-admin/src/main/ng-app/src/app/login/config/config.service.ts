import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {AuthServiceConfig, GoogleLoginProvider} from 'angularx-social-login';

@Injectable()
export class  ConfigService {

  constructor(private httpClient: HttpClient) {
  }

  getConfigProvider() {
     return this.httpClient.get('/api/config',  {responseType: 'text'});
  }
}
