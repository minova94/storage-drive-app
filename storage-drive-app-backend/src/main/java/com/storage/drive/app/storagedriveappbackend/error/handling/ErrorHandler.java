package com.storage.drive.app.storagedriveappbackend.error.handling;

public enum ErrorHandler {

    GOOGLE_SECURITY_EXCEPTION("001", "The verification of the signature failed");

    private final String id;
    private final String message;

    ErrorHandler(final String id, final String message) {
        this.id = id;
        this.message = message;
    }

    public String getId() {
        return id;
    }
    public String getMessage() {
        return message;
    }
}
