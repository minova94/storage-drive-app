package com.storage.drive.app.storagedriveappbackend.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServlet;

@Configuration
public class WebConfig {

    @Bean
    public ServletRegistrationBean<HttpServlet> driveServlet() {
        ServletRegistrationBean<HttpServlet> servRegBean = new ServletRegistrationBean<>();
        servRegBean.setServlet(new DriveServlet());
        servRegBean.addUrlMappings("/drive/*");
        servRegBean.setLoadOnStartup(1);
        return servRegBean;
    }
}
