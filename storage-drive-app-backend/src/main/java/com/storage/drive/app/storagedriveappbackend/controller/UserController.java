package com.storage.drive.app.storagedriveappbackend.controller;

import com.storage.drive.app.storagedriveappbackend.dto.UserDTO;
import com.storage.drive.app.storagedriveappbackend.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;

/**
 * Rest controller for user requests
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    /**
     * Authentication of the user
     *
     * @param idToken the token from the Google Provider
     * @return {@link UserDTO}
     */
    @PostMapping("/authenticate")
    public Boolean authenticate(@RequestBody String idToken) {
        LOGGER.info("Authenticate user with idToken: {}", idToken);

        UserDTO user = null;
        try {
            LOGGER.info("Verifying user with idToken: {}", idToken);
            user = userService.verifyUser(idToken);
            if (user != null) {
                LOGGER.info("Verified user with userId: {}, email: {}, verifiedEmail: {}, name: {}", user.getUserId(),
                        user.getEmail(), user.isEmailVerified(), user.getName());
                return user.isEmailVerified();
            }

        } catch (final GeneralSecurityException | IOException e) {
            LOGGER.error("Error exception occured: {}", e, e);
        }

        return false;
    }

}
