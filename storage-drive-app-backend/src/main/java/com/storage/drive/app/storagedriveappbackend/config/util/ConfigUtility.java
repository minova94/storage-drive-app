package com.storage.drive.app.storagedriveappbackend.config.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Utility for all configurations
 */
@Configuration
public class ConfigUtility {

    @Autowired
    private Environment environment;

    public String getProperty(final String property) {
        return environment.getProperty(property);
    }
}
