package com.storage.drive.app.storagedriveappbackend.dto;

/**
 * The User DTO
 */
public class UserDTO {

    private String userId;
    private String email;
    private boolean emailVerified;
    private String name;

    public UserDTO(String userId, String email, boolean emailVerified, String name) {
        this.userId = userId;
        this.email = email;
        this.emailVerified = emailVerified;
        this.name = name;
    }

    public String getUserId() {
    return userId;
}

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
