package com.storage.drive.app.storagedriveappbackend.service;

import com.storage.drive.app.storagedriveappbackend.dto.UserDTO;

import java.io.IOException;
import java.security.GeneralSecurityException;

public interface UserService {

    UserDTO verifyUser(final String idToken) throws GeneralSecurityException, IOException;
}
