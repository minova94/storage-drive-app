package com.storage.drive.app.storagedriveappbackend.controller;

import com.storage.drive.app.storagedriveappbackend.config.util.ConfigUtility;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Rest Controller for Configuration
 */
@RestController
@RequestMapping
public class ConfigProviderController {

    private static final Logger LOGGER = LogManager.getLogger(ConfigProviderController.class);

    @Autowired
    private ConfigUtility configUtility;

    /**
     * Get google client id from property
     * @return the client id
     */
    @GetMapping("/config")
    @Produces(MediaType.APPLICATION_JSON)
    public String getConfigProvider() {
        LOGGER.info("Get google clientId");
        return configUtility.getProperty("google.clientId");
    }

}
