package com.storage.drive.app.storagedriveappbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@ServletComponentScan
@SpringBootApplication
public class StorageDriveAppBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(StorageDriveAppBackendApplication.class, args);
	}

}
