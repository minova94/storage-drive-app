package com.storage.drive.app.storagedriveappbackend.service.impl;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.storage.drive.app.storagedriveappbackend.config.util.ConfigUtility;
import com.storage.drive.app.storagedriveappbackend.dto.UserDTO;
import com.storage.drive.app.storagedriveappbackend.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

/**
 * The service for the User
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    @Autowired
    private ConfigUtility configUtility;

    /**
     * Verification of the user
     * @param idToken the token from the Google Provider
     * @return {@link UserDTO}
     * @throws GeneralSecurityException
     * @throws IOException
     */
    public UserDTO verifyUser(final String idToken) throws GeneralSecurityException, IOException {

        LOGGER.info("Invoked method UserService.verifyUser with idToken: {}", idToken);

        UserDTO userDTO = null;

        GoogleIdTokenVerifier googleIdTokenVerifier = new GoogleIdTokenVerifier.Builder
                (new NetHttpTransport(), JacksonFactory.getDefaultInstance())
                .setAudience(Collections.singletonList(configUtility.getProperty("google.clientId"))).build();

        if (idToken != null) {
            LOGGER.info("The token is not empty");
            GoogleIdToken googleIdToken = googleIdTokenVerifier.verify(idToken);
            GoogleIdToken.Payload payload = googleIdToken.getPayload();
            LOGGER.info("Filling UserDTO with details");
            userDTO = new UserDTO(payload.getSubject(), payload.getEmail(), payload.getEmailVerified(), payload.get("name").toString());
        }
        LOGGER.info("Successfull verification of the user");
        return userDTO;
    }
}
