package com.storage.drive.app.storagedriveappbackend.service;

import java.io.IOException;
import java.security.GeneralSecurityException;

public interface DriveService {

    String findAllDriveFolders() throws GeneralSecurityException, IOException;
}
