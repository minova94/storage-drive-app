package com.storage.drive.app.storagedriveappbackend.config;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.storage.drive.app.storagedriveappbackend.service.impl.DriveServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

@WebServlet(urlPatterns = "/drive/*", loadOnStartup = 1)
public class DriveServlet extends HttpServlet {
    private static final String CREDENTIALS_FILE_PATH = "credentials.json";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE_METADATA_READONLY);
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    private NetHttpTransport getHttpTransport() throws GeneralSecurityException, IOException {
        return GoogleNetHttpTransport.newTrustedTransport();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try {
            Drive service = new Drive.Builder(getHttpTransport(), JSON_FACTORY, getCredentials(getHttpTransport()))
                    .setApplicationName("htssrhs")
                    .build();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

    }

    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        // Load client secrets.
        InputStream in = DriveServiceImpl.class.getClassLoader().getResourceAsStream(CREDENTIALS_FILE_PATH);
        if (in == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = null;
        try {
            flow = new GoogleAuthorizationCodeFlow.Builder(
                    getHttpTransport(), JSON_FACTORY, clientSecrets, SCOPES)
                    .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                    .setAccessType("offline")
                    .build();
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }

        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8081).build();
        return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");

    }

//    private static final Collection<String> SCOPES = Arrays.asList("email", "profile");
//    private static final JsonFactory JSON_FACTORY = new JacksonFactory();
//    private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
//
//    private GoogleAuthorizationCodeFlow flow;
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
//            throws IOException, ServletException {
//
//        String state = new BigInteger(130, new SecureRandom()).toString(32);  // prevent request forgery
//        req.getSession().setAttribute("state", state);
//
//        if (req.getAttribute("loginDestination") != null) {
//            req
//                    .getSession()
//                    .setAttribute("loginDestination", (String) req.getAttribute("loginDestination"));
//        } else {
//            req.getSession().setAttribute("loginDestination", "/books");
//        }
//
//        flow = new GoogleAuthorizationCodeFlow.Builder(
//                HTTP_TRANSPORT,
//                JSON_FACTORY,
//                getServletContext().getInitParameter("bookshelf.clientID"),
//                getServletContext().getInitParameter("bookshelf.clientSecret"),
//                SCOPES)
//                .build();
//
//        // Callback url should be the one registered in Google Developers Console
//        String url =
//                flow.newAuthorizationUrl()
//                        .setRedirectUri(getServletContext().getInitParameter("bookshelf.callback"))
//                        .setState(state)            // Prevent request forgery
//                        .build();
//        resp.sendRedirect(url);
//    }
}
