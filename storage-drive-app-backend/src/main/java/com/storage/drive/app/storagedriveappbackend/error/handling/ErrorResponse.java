package com.storage.drive.app.storagedriveappbackend.error.handling;

public class ErrorResponse {

    private ErrorHandler errorHandler;

    public ErrorResponse(final ErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
    }

    public ErrorHandler getErrorHandler() {
        return errorHandler;
    }
}
